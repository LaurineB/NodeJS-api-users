var express = require('express');
var router = express.Router();
var userList = [];
var viewPageCount = 0;

const request = require("request");
const url =
    "https://reqres.in/api/users";

router.all('*', function (req,res,next) {
    viewPageCount = viewPageCount +1;
    next();
});

/* GET users listing. */
router.get('/', function(req, res, next) {

    if(userList.length == 0) {
        request.get(url, function(error, response, body) {
            const json = JSON.parse(body);
            //res.send(json.data);
            const datas = json.data;

            for(var i = 0; i < datas.length; i++) {
                userList.push(datas[i]);
            }
            res.render('users',{datas: userList, views: viewPageCount});
        });
    } else {
        res.render('users',{datas: userList, views: viewPageCount});
    }


});

router.post('/', function(req, res, next) {

    userList.push(req.body);
    res.redirect('/users');
});

module.exports = router;

